/*global require*/

const {
  STYLE_WATCH_PATH,
  JS_WATCH_PATH,
  HTML_WATCH_PATH,
  WORK_OUT_FOLDER,
  PROD_FOLDER
} = require('./config.json');

//Gulp related dependencies
const gulp      = require('gulp'),
  gulpif        = require('gulp-if'),
  sass          = require('gulp-sass'),
  babel         = require('gulp-babel'),
  rename        = require('gulp-rename'),
  concat        = require('gulp-concat'),
  uglify        = require('gulp-uglify'),
  useref        = require('gulp-useref'),
  plumber       = require('gulp-plumber'),
  imagemin      = require('gulp-imagemin'),
  size          = require('gulp-filesize'),
  gulpStylelint = require('gulp-stylelint'),
  minifyCSS     = require('gulp-minify-css'),
  autoprefixer  = require('gulp-autoprefixer'),
  browserSync   = require('browser-sync').create(),
  reload = browserSync.reload,
  shell = require('gulp-shell');

gulp.task('browser-sync', () =>{
  browserSync.init({
    open:false,
    injectChanges:true,
    server: [WORK_OUT_FOLDER]
  })
})

gulp.task('js', () => {
  'use strict';

  const js = gulp.src([WORK_OUT_FOLDER + 'js/main.js'])
    .pipe(plumber())
});

gulp.task('vendors', () => {
  'use strict';

  const vendor_css = gulp.src([

  ])
    .pipe(concat('vendors.css'))
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'));

  const vendor_js = gulp.src([

  ])
    .pipe(babel())
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'));
});

gulp.task('images', () => {
  return gulp.src(WORK_OUT_FOLDER + 'images/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest(PROD_FOLDER + 'images'))
});

gulp.task('fonts', () => {
  return gulp.src(WORK_OUT_FOLDER + 'fonts/*.*')
    .pipe(gulp.dest(PROD_FOLDER + 'fonts'))
});

gulp.task('lint-css', () => {
  return gulp.src('src/**/*.scss')
    .pipe(gulpStylelint({
      reporters: [
        { formatter: 'string', console: true }
      ],
      debug: true
    }));
});

gulp.task('scss-to-css', ['lint-css'], () => {
  'use strict';

  const scss = gulp.src(WORK_OUT_FOLDER + 'scss/style.scss')
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'))
    .pipe(browserSync.stream());
});

gulp.task('default', ['scss-to-css','js']);

gulp.task('watch',['default', 'browser-sync'], () =>{
  gulp.watch( STYLE_WATCH_PATH, ['scss-to-css']);
  gulp.watch( JS_WATCH_PATH, ['js', reload]);
  gulp.watch( HTML_WATCH_PATH, reload);

});


gulp.task('bundle', ['scss-to-css', 'vendors', 'images', 'fonts'], () => {
  return gulp.src([WORK_OUT_FOLDER + '*.html'])
      .pipe(useref())
      .pipe(gulpif('main.js', uglify()))
      .pipe(gulpif('*.css', minifyCSS()))
      .pipe(gulp.dest(PROD_FOLDER))
      .pipe(size());
});